
var ready = function ( fn ) {
  if ( typeof fn !== 'function' ) return;
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {
  'use strict';

  var burger = document.querySelector('.burger');
  var navigationMenu = document.querySelector('.main-nav');

  burger.addEventListener('click', function (event) {

    event.preventDefault();

    if (navigationMenu.classList.contains('main-nav--is-active')) {
      navigationMenu.classList.remove('main-nav--is-active');
      return;
    }

    navigationMenu.classList.add('main-nav--is-active');
  });



  validate.init({
    messageValueMissing: 'Заполните поле',
    messageTypeMismatchEmail: 'Введите e-mail'
  });



  var allInputs = document.querySelectorAll('input');

  allInputs.forEach(function (input, index) {
    if (!input.classList.contains('checkbox__hidden')) {
      input.addEventListener('input', function () {
        if (input.value.trim().length > 0) {
          input.classList.add('is-active');
        } else {
          input.classList.remove('is-active');
        }
      });
    }
  });



  window.addEventListener("DOMContentLoaded", function() {
    function setCursorPosition(pos, elem) {
      elem.focus();
      if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
          var range = elem.createTextRange();
          range.collapse(true);
          range.moveEnd("character", pos);
          range.moveStart("character", pos);
          range.select()
      }
  }

  function mask(event) {

    var matrix = "+7 (___) ___-__-__",
        i = 0,
        def = matrix.replace(/\D/g, ""),
        val = this.value.replace(/\D/g, "");

    if (def.length >= val.length) val = def;
    this.value = matrix.replace(/./g, function(a) {
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
    });

    if (event.type == "blur") {
      if (this.value.length == 2) this.value = ""
      } else setCursorPosition(this.value.length, this)
    };

    var input = document.querySelector("#phone-number");

    input.addEventListener("input", mask, false);
    input.addEventListener("focus", mask, false);
    input.addEventListener("blur", mask, false);
  });



  $('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });
  });
});